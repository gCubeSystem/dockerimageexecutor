# Docker Image Executor

This is a simple algorithm that execute a image docker on a Swarm Cluster using a registry.

```
python3 dockerimageexecutor.py <registry/image> <software-execute-command-name> <file-item-id> 

```

 
## Example

```
python3 dockerimageexecutor.py "microservices-VirtualBox:443/sortapp" sortapp 548eade8-25cf-4978-9f61-0f0c652900be

```

## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

