#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2018/06/15 
# 
import uuid

from .storagehubcommand import StorageHubCommand
from .storagehubcommandrootitemid import StorageHubCommandRootItemId
from .storagehubcommandcreatefolder import StorageHubCommandCreateFolder


class StorageHubCommandCreateTempFolder(StorageHubCommand):     

    def __init__(self, gcubeToken, storageHubUrl): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        self.tempFolderName = "DataMinerResult" 
        self.tempFolderDescription = "DataMiner temporary result folder"
        self.tempFolderHidden = "false"
        
    def execute(self):
        print("Execute StorageHubCommandCreateTempFolder")

        print("Retrieve RootFolder")
        cmdRootItemId = StorageHubCommandRootItemId(self.gcubeToken, self.storageHubUrl)  
        rootFolderItemId = cmdRootItemId.execute()
        
        print("Create Temp Folder Name")
        tempFolderPostfix = uuid.uuid4().hex
        self.tempFolderName = self.tempFolderName + tempFolderPostfix
        print("Temp Folder Name: " + str(self.tempFolderName))
        
        cmdCreateFolder = StorageHubCommandCreateFolder(self.gcubeToken, self.storageHubUrl, rootFolderItemId,
                                                             self.tempFolderName, self.tempFolderDescription, self.tempFolderHidden)
        tempFolderItemId = cmdCreateFolder.execute()
        
        print("Temp Folder Item Id: " + str(tempFolderItemId))
        
        return tempFolderItemId
    
    def __str__(self): 
        return 'StorageHubCommandCreateTempFolder[storageHubUrl=' + str(self.storageHubUrl) + ']' 
