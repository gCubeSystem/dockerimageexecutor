#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2018/06/15 
# 
import requests

from .storagehubcommand import StorageHubCommand


class StorageHubCommandCreateFolder(StorageHubCommand):     

    def __init__(self, gcubeToken, storageHubUrl, folderItemId, folderName, folderDescription, folderHidden="false"): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        self.folderItemId = folderItemId
        self.folderName = folderName
        self.folderDescription = folderDescription
        self.folderHidden = folderHidden
        
    def execute(self):
        print("Execute StorageHubCommandCreateFolder")
        print(self.storageHubUrl + "/items/" + self.folderItemId + "/create/FOLDER?");
            
        folderdata = {'name': self.folderName, 'description': self.folderDescription, 'hidden': self.folderHidden}
        print(str(folderdata))
        
        urlString = self.storageHubUrl + "/items/" + self.folderItemId + "/create/FOLDER?gcube-token=" + self.gcubeToken
        r = requests.post(urlString, data=folderdata)
        print(r)
        print(r.status_code)
        if r.status_code != 200:
            print("Error in execute StorageHubCommandCreateFolder: " + r.status_code)
            raise Exception("Error in execute StorageHubCommandCreateFolder: " + r.status_code)
        print('Created Folder ItemId: ' + str(r.text))
        return r.text

    def __str__(self): 
        return ('StorageHubCommandItemUpload[ storageHubUrl=' + str(self.storageHubUrl) + 
                ' folderItemId=' + str(self.folderItemId) + 
                ', folderName=' + str(self.folderName) + 
                ', folderDescription=' + str(self.folderDescription) + 
                ', folderHidden=' + str(self.folderHidden) + ']') 
