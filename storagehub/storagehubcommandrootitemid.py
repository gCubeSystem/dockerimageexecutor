#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2018/06/15 
# 
import requests
import json
from .storagehubcommand import StorageHubCommand


class StorageHubCommandRootItemId(StorageHubCommand):     

    def __init__(self, gcubeToken, storageHubUrl): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        
    def execute(self):
        print("Execute StorageHubCommandRootItemId")
        print(self.storageHubUrl + "/?exclude=hl:accounting");
        urlString = self.storageHubUrl + "/?exclude=hl:accounting&gcube-token=" + self.gcubeToken
        r = requests.get(urlString)
        print(r.status_code)
        if r.status_code != 200:
            print("Error in execute StorageHubCommandRootItemId: " + r.status_code)
            raise Exception("Error in execute StorageHubCommandRootItemId: " + r.status_code)

        rootItemJ = json.loads(r.text)
        rootId = rootItemJ['item']['id']
        print('RootItemId:' + str(rootId))
        return rootId
    
    def __str__(self): 
        return 'StorageHubCommandRootItemId[storageHubUrl=' + str(self.storageHubUrl) + ']' 
