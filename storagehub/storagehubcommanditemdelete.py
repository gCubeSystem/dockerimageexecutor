#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2018/06/15 
# 
import requests
from .storagehubcommand import StorageHubCommand


class StorageHubCommandItemDelete(StorageHubCommand):     

    def __init__(self, gcubeToken, storageHubUrl, itemId, permanently="false"): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        self.itemId = itemId
        self.permanently = permanently
        
    def execute(self):
        print("Execute StorageHubCommandItemDelete")
        print(self.storageHubUrl + "/items/" + self.itemId + "?force=" + self.permanently);
        
        urlString = self.storageHubUrl + "/items/" + self.itemId + "?force=" + self.permanently + "&gcube-token=" + self.gcubeToken
        r = requests.delete(urlString)
        print(r.status_code)
        if r.status_code != 200:
            print("Error in execute StorageHubCommandItemDelete: " + r.status_code)
            raise Exception("Error in execute StorageHubCommandItemDelete: " + r.status_code)  

    def __str__(self): 
        return ('StorageHubCommandItemDelete[storageHubUrl=' + str(self.storageHubUrl) + 
                ', itemId=' + self.itemId + 
                ', permanently=' + self.permanently + ']') 
