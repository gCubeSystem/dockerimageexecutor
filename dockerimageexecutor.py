#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2020/06/12 
# 
import os
import sys

from issupport import ISSupport
from engine.dockerengine import DockerEngine
from engine.swarmengine import SwarmEngine
from storagehub.storagehubcommanditemdownload import StorageHubCommandItemDownload
from storagehub.storagehubcommandcreatetempfolder import StorageHubCommandCreateTempFolder
from storagehub.storagehubcommanditemdelete import StorageHubCommandItemDelete


class DockerImageExecutor: 

    def __init__(self): 
        self.resultFile = "result.zip"
        self.globalVariablesFile = "globalvariables.csv"
        self.gcubeToken = None
        self.storageHubUrl = None
        self.softwareImage = sys.argv[1] # Software Image
        self.softwareExecuteCommandName = sys.argv[2] # Command to Run
        self.itemId = sys.argv[3]  # Input Data Item 
        self.tempFolderItemId = None  # '32c0422f-a777-4452-adea-007347ec4484'
        
    def main(self):
        print(self)
        self.retrieveToken()
        issup = ISSupport()
        self.storageHubUrl = issup.discoverStorageHub(self.gcubeToken)
        self.createTempFolder()
        # self.executeOnDocker()
        self.executeOnSwarm()
        
    def retrieveToken(self): 
        print("Retrieve gcubeToken")
        if not os.path.isfile(self.globalVariablesFile):
            print("File does not exist: " + self.globalVariablesFile)
            raise Exception("File does not exist: " + self.globalVariablesFile)  
        with open(self.globalVariablesFile) as fp:
            for line in fp:
                if line.find("gcube_token") != -1:
                    tk = line[14:]
                    self.gcubeToken = tk.replace('"', '').strip()
                    print("Found gcube_token")
                    break
        if self.gcubeToken == None:
            print('Error gcube_token not found!')
            raise Exception('Error gcube_token not found!')      
    
    def createTempFolder(self): 
        print("Create Temp Folder")
        cmdCreateTempFolder = StorageHubCommandCreateTempFolder(self.gcubeToken, self.storageHubUrl)
        self.tempFolderItemId = cmdCreateTempFolder.execute()
        
    def deleteTempFolder(self):
        print("Delete Temp Folder")
        cmdDeleteTempFolder = StorageHubCommandItemDelete(self.gcubeToken, self.storageHubUrl, self.tempFolderItemId)
        cmdDeleteTempFolder.execute()
        
    def downloadResults(self): 
        print("Get Results")
        cmdItemDownload = StorageHubCommandItemDownload(self.gcubeToken, self.storageHubUrl,
                                                        self.tempFolderItemId, self.resultFile)
        cmdItemDownload.execute()
        self.deleteTempFolder()
    
    def executeOnDocker(self):
        print("Execute On Docker")
        dEngine = DockerEngine(self.gcubeToken, self.storageHubUrl,
                          self.softwareImage, self.softwareExecuteCommandName, self.itemId, self.tempFolderItemId)
        dEngine.execute()
        self.downloadResults()
        
    def executeOnSwarm(self):
        print("Execute On Swarm")
        sEngine = SwarmEngine(self.gcubeToken, self.storageHubUrl,
                          self.softwareImage, self.softwareExecuteCommandName, self.itemId, self.tempFolderItemId)
        sEngine.execute()
        self.downloadResults()    
        
    def __str__(self): 
        return 'DockerImageExecutor' 


def main():
    print('docker_image_executor')
    dockerImageExecutor = DockerImageExecutor()
    dockerImageExecutor.main()

    
main()

