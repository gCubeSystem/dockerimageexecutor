#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2020/06/12 
# 
import docker
import json
import gzip


class DockerEngine:     

    def __init__(self, gcubeToken, storageHubUrl, softwareImage, softwareExecuteCommandName, itemId, tempFolderItemId): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        
        # Software image for example: "microservices-VirtualBox:443/sortapp-img"
        self.softwareImage = softwareImage 
        # Software Execute Command Name for example: "sortapp"
        self.softwareExecuteCommandName = softwareExecuteCommandName
       
        # Input Data Item
        self.itemId = itemId
        self.tempFolderItemId = tempFolderItemId
        self.dockerImageArchive = "sortapp-img.tar.gz"
        
    def execute(self):
        print("Execute DockerImageEngine")
        
        print("Create Client")
        
        # TLS support
        # tls_config = docker.tls.TLSConfig(
        # client_cert=('/path/to/client-cert.pem', '/path/to/client-key.pem')
        # )
        # client = docker.DockerClient(base_url='<https_url>', tls=tls_config)
        
        client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        print(json.dumps(client.info(), indent=2))
        # print(json.dumps(client.version(), indent=2))
        
        print("Add File Image")
        with gzip.open(self.dockerImageArchive, "rb") as imageZip:
            imagedata = imageZip.read()
            sortAppImg = client.images.load(imagedata)
            print(sortAppImg)
        
        imagesList = client.images.list(all=True)
        print ("Images found: ")
        print(*imagesList, sep="\n") 
        
        print("Create Container")
        cmdValue = "{} {} {} {}".format(self.softwareExecuteCommandName, self.gcubeToken, self.itemId, self.tempFolderItemId)
        print("CommandValue: " + cmdValue)
        hostConf = client.api.create_host_config(auto_remove=True);
        container = client.api.create_container(
            image='sortapp-img:latest',
            command=cmdValue,
            host_config=hostConf)
        
        print("Start Container")
        client.api.start(container=container.get('Id'))
        client.api.wait(container=container.get('Id'), timeout=3600)
        print("Container Execution End")
       
    def __str__(self): 
        return ('DEngine[storageHubUrl=' + str(self.storageHubUrl) + 
                ', softwareImage=' + str(self.softwareImage) + 
                ', softwareExecuteCommandName=' + str(self.softwareExecuteCommandName) + 
                ', itemId=' + str(self.itemId) + 
                ', tempFolderItemId=' + str(self.tempFolderItemId) + ']')
