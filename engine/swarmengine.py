#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2020/06/12 
# 
import docker
import json
import time
from docker.types.services import RestartPolicy


class SwarmEngine:     

    def __init__(self, gcubeToken, storageHubUrl, softwareImage, softwareExecuteCommandName, itemId, tempFolderItemId): 
        self.gcubeToken = gcubeToken
        self.storageHubUrl = storageHubUrl
        
        # Software image for example: "microservices-VirtualBox:443/sortapp"
        self.softwareImage = softwareImage
        # Software Execute Command Name for example: "sortapp"
        self.softwareExecuteCommandName = softwareExecuteCommandName
        
        # Input Data Item
        self.itemId = itemId
        self.tempFolderItemId = tempFolderItemId
        
        # Client Base URL
        # 'http://docker-swarm1.int.d4science.net:2376/'
        # 'https://swarm.d4science.org:2375/'
        self.clientBaseUrl='http://docker-swarm1.int.d4science.net:2376/'
       
    
    def monitorTask(self, srv):
        print("Monitor Task")

        end = False
        sId=None
        
        while not end:
            for task in srv.tasks():
                print("Task: " + str(task))
                sId = task['ServiceID']
                if sId and sId == srv.id:
                    status = task['DesiredState']
                    print("Task DesiredState: " + str(status))
                    if status == 'shutdown' or status == 'complete':
                        print("Task End")
                        srv.remove()
                        print("Service Removed")
                        end = True
                    elif (status == 'failed' or status == 'rejected' 
                          or status == 'orphaned' or status == 'remove'):  
                            print("Error in execute Docker Image on Swarm Node: " + str(status))
                            raise Exception("Error in execute Docker Image on Swarm Node: " + str(status))               
                    else:
                        time.sleep(2)
                else:
                    continue
            if sId==None:    
                print("Waiting Task load on Service")
                time.sleep(2)
            
    def execute(self):
        print("Execute SwarmRegistryEngine")

        print("Create Client")
    
        # DockerClient 
        # for example: http://microservices-VirtualBox:2375/
        # 
        # TLS support
        # tls_config = docker.tls.TLSConfig(
        # client_cert=('/path/to/client-cert.pem', '/path/to/client-key.pem')
        # )
        # client = docker.DockerClient(base_url='<https_url>', tls=tls_config)
        
        
        print(self.clientBaseUrl)
        
        client = docker.DockerClient(base_url=self.clientBaseUrl)
         
        # client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        print(json.dumps(client.info(), indent=2))

        #image=client.images.pull(self.softwareImage, tag='latest')
        
        #print("Pulled: "+str(image))

        print("Services: " + str(client.services.list()))

        cmdValue = "{} {} {} {}".format(self.softwareExecuteCommandName, self.gcubeToken, self.itemId, self.tempFolderItemId)
        print("CommandValue: " + cmdValue)
        
        constraintsList=["node.role==worker"]
        print("Contraints: ") 
        print(*constraintsList, sep = "\n") 
        
        
        print("Create Service")

        srv = client.services.create(self.softwareImage, command=cmdValue, constraints=constraintsList,
                restart_policy=RestartPolicy(condition='none', delay=0, max_attempts=0, window=0))

        print("Service: " + str(srv))

        print("Service Id: " + str(srv.id))

        print("Node: " + str(client.nodes.list()))
        
        self.monitorTask(srv)
                        
        print("Service Execution End")

    def __str__(self):
        return ('SwarmRegistryEngine[storageHubUrl=' + str(self.storageHubUrl) + 
                ', softwareImage=' + str(self.softwareImage) + 
                ', softwareExecuteCommandName=' + str(self.softwareExecuteCommandName) + 
                ', itemId=' + str(self.itemId) + 
                ', tempFolderItemId=' + str(self.tempFolderItemId) + ']')

