#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
#
# Created on 2020/06/12 
# 

from threading import Thread


class DMonitor (Thread):

    def __init__(self, name, interval, client):
        Thread.__init__(self)
        self.name = name
        self.interval = interval
        self.client = client
        self.end = False
        self.count = 0
        
    def conclude(self):
        print("Call End")
        self.end = True
                  
    def run(self):
        print ("Thread '" + self.name + "' start") 
        for event in self.client.events(decode=True):
            print(event)
            if self.end:
                break
        print ("Thread '" + self.name + "' end")
        